﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ships.ViewModels;
using Ships.Models;
using Ships.Repositories;

namespace Ships.Services
{
    public interface IGameManagementService
    {
        bool CheckIfPlayersAlreadyPlay(GameViewModel model);
        bool CheckIfUserPlayed(string userName);
        void CreateGame(GameViewModel model);
    }
    public class GameManagementService : IGameManagementService
    {
        private IPlayerService _playerService;
        private IGameRepository _gameRepository;

        public GameManagementService(
            IPlayerService playerService,
            IGameRepository gameRepository  
        ){
            _playerService = playerService;
            _gameRepository = gameRepository;
        }

        public bool CheckIfUserPlayed(string userName)
        {
            return false;
        }

        public bool CheckIfPlayersAlreadyPlay(GameViewModel model)
        {
            var firstPlayerId = _playerService.GetPlayerIdByName(model.FirstPlayerName);
            var secondPlayerId = _playerService.GetPlayerIdByName(model.SecondPlayerName);

            return _gameRepository.CheckIfGameExists(firstPlayerId, secondPlayerId);
        }

        public void CreateGame(GameViewModel model)
        {
            var firstPlayerId = _playerService.GetPlayerIdByName(model.FirstPlayerName);
            var secondPlayerId = _playerService.GetPlayerIdByName(model.SecondPlayerName);

            _gameRepository.InsertNewGameEntry(firstPlayerId, secondPlayerId);
        }
    }
}