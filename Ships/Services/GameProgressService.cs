﻿using Ships.Repositories;
using Ships.ViewModels;
using Ships.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ships.Services
{
    public interface IGameProgressService
    {
        bool EraseUserGameProgress(string userName);
        void EraseUsersGameData(int gameId, int firstPlayerId, int secondPlayerId);
        void SavePlayerShot(GameViewModel viewModel);
        IEnumerable<GameProgress> GetPlayerProgress(int gameId, int playerId);
        bool CheckIfPlayerWon(int gameId, int playerId);
    }
    public class GameProgressService : IGameProgressService
    {
        private IGameProgressRepository _gameProgressRepository;
        private IPlayerService _playerService;

        public GameProgressService(
            IGameProgressRepository gameProgressRepository,
            IPlayerService playerService
        ){
            _gameProgressRepository = gameProgressRepository;
            _playerService = playerService;
        }

        public bool EraseUserGameProgress(string userName)
        {
            return true;
        }

        public void SavePlayerShot(GameViewModel viewModel)
        {
            var opponentShipsCoords = _playerService.GetPlayerShipsCoords(viewModel.CurrentGameId, viewModel.OpponentPlayerId);

            var shotResult = CheckIfShotSuccessful(opponentShipsCoords, viewModel.currentShotX, viewModel.currentShotY);

            _gameProgressRepository.InsertGameProgressEntry(new GameProgress
            {
                GameId = viewModel.CurrentGameId,
                PlayerId = viewModel.CurrentPlayerId,
                PosX = viewModel.currentShotX,
                PosY = viewModel.currentShotY,
                ShotResult = shotResult
            });
        }

        private int CheckIfShotSuccessful(IEnumerable<PlayerShip> opponentShipsCoords, int posX, int posY)
        {
            foreach(var item in opponentShipsCoords)
            {
                if(item.ShipCoord.Equals($"{posX.ToString()},{posY.ToString()}"))
                {
                    return 1;
                }
            }

            return 0;
        }

        public IEnumerable<GameProgress> GetPlayerProgress(int gameId, int playerId)
            => _gameProgressRepository.GetPlayerProgress(gameId, playerId);

        public bool CheckIfPlayerWon(int gameId, int playerId)
        {
            var hitedShipsAmount = GetPlayerProgress(gameId, playerId).Where(x => x.ShotResult == 1).Count();

            if(hitedShipsAmount == 5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void EraseUsersGameData(int gameId, int firstPlayerId, int secondPlayerId)
        {
            _gameProgressRepository.EraseUsersGameData(gameId, firstPlayerId, secondPlayerId);
        }
    }
}