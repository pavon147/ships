﻿using Ships.Models;
using Ships.Repositories;
using Ships.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ships.Services
{
    public interface IGameDataService
    {
        void InsertShipsForPlayers(Dictionary<string, PlayerShip[]> allPlayersShips, GameViewModel viewModel);
        int GetGameIdByPlayersId(int firstPlayerId, int secondPlayerId);
    }
    public class GameDataService : IGameDataService
    {
        private IGameRepository _gameRepository;
        private IPlayerRepository _playerRepository;
        private IPlayerShipRepository _playerShipRepository;

        public GameDataService(
            IGameRepository gameRepository,
            IPlayerRepository playerRepository,
            IPlayerShipRepository playerShipRepository
        ){
            _gameRepository = gameRepository;
            _playerRepository = playerRepository;
            _playerShipRepository = playerShipRepository;
        }

        public void InsertShipsForPlayers(Dictionary<string, PlayerShip[]> allPlayersShips, GameViewModel viewModel)
        {
            var firstPlayerShips = allPlayersShips.FirstOrDefault(x => x.Key == "FirstPlayerShips").Value;
            var secondPlayerShips = allPlayersShips.FirstOrDefault(x => x.Key == "SecondPlayerShips").Value;

            var firstPlayerId = _playerRepository.GetPlayerIdByName(viewModel.FirstPlayerName);
            var secondPlayerId = _playerRepository.GetPlayerIdByName(viewModel.SecondPlayerName);

            var currentGameId = GetGameIdByPlayersId(firstPlayerId, secondPlayerId);

            foreach(var item in firstPlayerShips)
            {
                _playerShipRepository.InsertPlayerShipsCoords(new PlayerShip
                {
                    GameId = currentGameId,
                    PlayerId = firstPlayerId,
                    ShipCoord = item.ShipCoord
                });
            }

            foreach (var item in secondPlayerShips)
            {
                _playerShipRepository.InsertPlayerShipsCoords(new PlayerShip
                {
                    GameId = currentGameId,
                    PlayerId = secondPlayerId,
                    ShipCoord = item.ShipCoord
                });
            }
        }

        public int GetGameIdByPlayersId(int firstPlayerId, int secondPlayerId)
            => _gameRepository.GetGameIdByPlayersId(firstPlayerId, secondPlayerId);
    }
}