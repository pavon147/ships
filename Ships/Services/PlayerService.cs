﻿using Ships.Models;
using Ships.Repositories;
using Ships.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ships.Services
{
    public interface IPlayerService
    {
        bool CheckIfPlayerExists(string playerName);
        bool CheckIfPlayersExist(GameViewModel viewModel);
        bool AddPlayer(string playerName);
        int GetPlayerIdByName(string playerName);
        void ProvideAccountsForPlayers(GameViewModel viewModel);
        IEnumerable<PlayerShip> GetPlayerShipsCoords(int gameId, int playerId);
    }
    public class PlayerService : IPlayerService
    {
        private IPlayerRepository _playerRepository;
        private IPlayerShipRepository _playerShipRepository;

        public PlayerService(
            IPlayerRepository playerRepository,
            IPlayerShipRepository playerShipRepository
        ){
            _playerRepository = playerRepository;
            _playerShipRepository = playerShipRepository;
        }

        public bool AddPlayer(string playerName)
        {
            return true;
        }

        public bool CheckIfPlayerExists(string playerName)
        {
            return false;
        }

        public bool CheckIfPlayersExist(GameViewModel viewModel)
        {
            var firstPlayerId = GetPlayerIdByName(viewModel.FirstPlayerName);
            var secondPlayerId = GetPlayerIdByName(viewModel.SecondPlayerName);

            if(firstPlayerId != 0 && secondPlayerId != 0)
            {
                return true;
            }

            return false;
        }

        public int GetPlayerIdByName(string playerName)
            => _playerRepository.GetPlayerIdByName(playerName);

        public void ProvideAccountsForPlayers(GameViewModel viewModel)
        {
            if(GetPlayerIdByName(viewModel.FirstPlayerName) == 0)
            {
                _playerRepository.CreatePlayerAccount(new Player
                {
                    Name = viewModel.FirstPlayerName
                });
            }
            if (GetPlayerIdByName(viewModel.SecondPlayerName) == 0)
            {
                _playerRepository.CreatePlayerAccount(new Player
                {
                    Name = viewModel.SecondPlayerName
                });
            }
        }

        public IEnumerable<PlayerShip> GetPlayerShipsCoords(int gameId, int playerId)
            => _playerShipRepository.GetPlayerShipCoords(gameId, playerId);
    }
}