﻿using Ships.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ships.Services
{
    public interface IGameViewModelService
    {
        GameViewModel SwapCurrentPlayers(GameViewModel viewModel);
    }
    public class GameViewModelService : IGameViewModelService
    {
        public GameViewModel SwapCurrentPlayers(GameViewModel viewModel)
        {
            var playerIdTemp = viewModel.CurrentPlayerId;
            var playerNameTemp = viewModel.CurrentPlayerName;

            viewModel.CurrentPlayerId = viewModel.OpponentPlayerId;
            viewModel.OpponentPlayerId = playerIdTemp;

            viewModel.CurrentPlayerName = viewModel.OpponentPlayerName;
            viewModel.OpponentPlayerName = playerNameTemp;

            return viewModel;
        }
    }
}