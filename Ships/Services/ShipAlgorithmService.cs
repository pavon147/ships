﻿using Ships.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ships.Services
{
    public interface IShipAlgorithmService
    {
        Dictionary<string, PlayerShip[]> GetShipsCoordsForPlayers();
    }
    public class ShipAlgorithmService : IShipAlgorithmService
    {
        private PlayerShip[] _firstPlayerShips;
        private PlayerShip[] _secondPlayerShips;
        private Dictionary<string, PlayerShip[]> _shipsForPlayers;
        private Random _random;

        public ShipAlgorithmService()
        {
            _random = new Random();
            _shipsForPlayers = new Dictionary<string, PlayerShip[]>();
            _firstPlayerShips = new PlayerShip[5];
            _secondPlayerShips = new PlayerShip[5];
        }

        public Dictionary<string, PlayerShip[]> GetShipsCoordsForPlayers()
        {
            for (int i = 0; i < 5; i++)
            {
                _firstPlayerShips[i] = new PlayerShip { ShipCoord = GetProperShipCoords() };
                _secondPlayerShips[i] = new PlayerShip { ShipCoord = GetProperShipCoords() };
            }

            _shipsForPlayers.Add("FirstPlayerShips", _firstPlayerShips);
            _shipsForPlayers.Add("SecondPlayerShips", _secondPlayerShips);

            return _shipsForPlayers;
        }

        private string GetProperShipCoords()
        {
            string posX;
            string posY;
            string coords;

            do
            {
                posX = _random.Next(1, 5).ToString();
                posY = _random.Next(1, 5).ToString();

                coords = posX + ',' + posY;
            }
            while (_firstPlayerShips.Any(x => x?.ShipCoord == coords) || _secondPlayerShips.Any(x => x?.ShipCoord == coords));

            return coords;
        }
    }
}