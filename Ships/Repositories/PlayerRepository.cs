﻿using Ships.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ships.Repositories
{
    public interface IPlayerRepository
    {
        int GetPlayerIdByName(string playerName);
        void CreatePlayerAccount(Player playerEntry);
    }
    public class PlayerRepository : IPlayerRepository
    {
        public int GetPlayerIdByName(string playerName)
        {
            using (var dbContext = new shipsEntities())
            {
                if(dbContext.Player.Any(x => x.Name == playerName))
                {
                    return dbContext.Player.FirstOrDefault(x => x.Name == playerName).Id;
                }
                else
                {
                    return 0;
                }
            }
        }

        public void CreatePlayerAccount(Player playerEntry)
        {
            using (var dbContext = new shipsEntities())
            {
                dbContext.Player.Add(playerEntry);

                dbContext.SaveChanges();
            }
        }
    }
}