﻿using Ships.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ships.Repositories
{
    public interface IGameRepository
    {
        bool CheckIfGameExists(int firstPlayerId, int secondPlayerId);
        void InsertNewGameEntry(int firstPlayerId, int secondPlayerId);
        int GetGameIdByPlayersId(int firstPlayerId, int secondPlayerId);

    }
    public class GameRepository : IGameRepository
    {
        public bool CheckIfGameExists(int firstPlayerId, int secondPlayerId)
        {
            using (var dbContext = new shipsEntities())
            {
                return dbContext.Game.Any(x => x.FirstPlayerId == firstPlayerId && x.SecondPlayerId == secondPlayerId);
            }
        }

        public void InsertNewGameEntry(int firstPlayerId, int secondPlayerId)
        {
            using (var dbContext = new shipsEntities())
            {
                dbContext.Game.Add(new Game
                {
                    FirstPlayerId = firstPlayerId,
                    SecondPlayerId = secondPlayerId,
                    GameStatusId = 1
                });

                dbContext.SaveChanges();
            }
        }

        public int GetGameIdByPlayersId(int firstPlayerId, int secondPlayerId)
        {
            using (var dbContext = new shipsEntities())
            {
                if(dbContext.Game.Any(x => x.FirstPlayerId == firstPlayerId && x.SecondPlayerId == secondPlayerId))
                {
                    return dbContext.Game.FirstOrDefault(x => x.FirstPlayerId == firstPlayerId && x.SecondPlayerId == secondPlayerId).Id;
                }

                return 0;
            }
        }
    }
}