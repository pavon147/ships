﻿using Ships.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ships.Repositories
{
    public interface IPlayerShipRepository
    {
        void InsertPlayerShipsCoords(PlayerShip playerShipEntry);
        IEnumerable<PlayerShip> GetPlayerShipCoords(int gameId, int playerId);
    }
    public class PlayerShipRepository : IPlayerShipRepository
    {     
        public void InsertPlayerShipsCoords(PlayerShip playerShipEntry)
        {
            using (var dbContext = new shipsEntities())
            {
                dbContext.PlayerShip.Add(playerShipEntry);

                dbContext.SaveChanges();
            }
        }

        public IEnumerable<PlayerShip> GetPlayerShipCoords(int gameId, int playerId)
        {
            using (var dbContext = new shipsEntities())
            {
                return dbContext.PlayerShip.Where(x => x.GameId == gameId && x.PlayerId == playerId).ToList();
            }
        }
    }
    
}