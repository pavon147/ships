﻿using Ships.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ships.Repositories
{
    public interface IGameProgressRepository
    {
        void InsertGameProgressEntry(GameProgress gameProgressEntry);
        IEnumerable<GameProgress> GetPlayerProgress(int gameId, int playerId);
        void EraseUsersGameData(int gameId, int firstPlayerId, int secondPlayerId);
    }
    public class GameProgressRepository : IGameProgressRepository
    {        
        public void InsertGameProgressEntry(GameProgress gameProgressEntry)
        {
            using (var dbContext = new shipsEntities())
            {
                dbContext.GameProgress.Add(gameProgressEntry);

                dbContext.SaveChanges();
            }
        }

        public IEnumerable<GameProgress> GetPlayerProgress(int gameId, int playerId)
        {
            using (var dbContext = new shipsEntities())
            {
                return dbContext.GameProgress.Where(x => x.GameId == gameId && x.PlayerId == playerId).ToList();
            }
        }

        public void EraseUsersGameData(int gameId, int firstPlayerId, int secondPlayerId)
        {
            using (var dbContext = new shipsEntities())
            {
                var playersShipsToDelete = dbContext.PlayerShip.Where(x => x.GameId == gameId && (x.PlayerId == firstPlayerId || x.PlayerId == secondPlayerId)).ToList();

                foreach (var item in playersShipsToDelete)
                {
                    dbContext.PlayerShip.Remove(item);
                }

                dbContext.SaveChanges();

                var playersProgressToDelete = dbContext.GameProgress.Where(x => x.GameId == gameId && (x.PlayerId == firstPlayerId || x.PlayerId == secondPlayerId)).ToList();

                foreach (var item in playersProgressToDelete)
                {
                    dbContext.GameProgress.Remove(item);
                }

                dbContext.SaveChanges();

                var currentGameToDelete = dbContext.Game.FirstOrDefault(x => x.Id == gameId);

                dbContext.Game.Remove(currentGameToDelete);
                

                dbContext.SaveChanges();
            }
        }
    }
}