﻿using Ships.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ships.ViewModels
{
    public class GameViewModel
    {
        public int FirstPlayerId { get; set; }
        public int SecondPlayerId { get; set; }
        [Required]
        [DisplayName("First Player Name")]
        public string FirstPlayerName { get; set; }
        [Required]
        [DisplayName("Second Player Name")]
        public string SecondPlayerName { get; set; }
        public PlayerShip[] FirstPlayerShipsCoords { get; set; }
        public PlayerShip[] SecondPlayerShipsCoords { get; set; }
        public int CurrentGameId { get; set; }
        public int CurrentPlayerId { get; set; }
        public string CurrentPlayerName { get; set; }
        public IEnumerable<GameProgress> currentPlayerProgress { get; set; }
        public int OpponentPlayerId { get; set; }
        public string OpponentPlayerName { get; set; }
        [Required]
        [DisplayName("X Position")]
        public int currentShotX { get; set; }
        [Required]
        [DisplayName("Y Position")]
        public int currentShotY { get; set; }

        public bool GameFinished { get; set; }

    }
}