﻿using Autofac;
using Autofac.Integration.Mvc;
using Ships.Repositories;
using Ships.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace Ships
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<GameManagementService>().As<IGameManagementService>();
            builder.RegisterType<GameProgressService>().As<IGameProgressService>();
            builder.RegisterType<PlayerService>().As<IPlayerService>();
            builder.RegisterType<ShipAlgorithmService>().As<IShipAlgorithmService>();
            builder.RegisterType<GameDataService>().As<IGameDataService>();
            builder.RegisterType<GameViewModelService>().As<IGameViewModelService>();

            builder.RegisterType<PlayerRepository>().As<IPlayerRepository>();
            builder.RegisterType<GameRepository>().As<IGameRepository>();
            builder.RegisterType<PlayerShipRepository>().As<IPlayerShipRepository>();
            builder.RegisterType<GameProgressRepository>().As<IGameProgressRepository>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
