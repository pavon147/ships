﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ships.Models;
using Ships.ViewModels;
using Ships.Services;

namespace Ships.Controllers
{
    public class GameController : Controller
    {
        private IGameManagementService _gameManagementService;
        private IGameProgressService _gameProgressService;
        private IGameDataService _gameDataService;
        private IPlayerService _playerService;
        private IShipAlgorithmService _shipAlgorithmService;
        private IGameViewModelService _gameViewModelService;

        public GameController(
            IGameManagementService gameManagementsService,
            IGameProgressService gameProgressService,
            IGameDataService gameDataService,
            IPlayerService playerService,
            IShipAlgorithmService shipAlgorithmService,
            IGameViewModelService gameViewModelService
        ){
            _gameManagementService = gameManagementsService;
            _gameProgressService = gameProgressService;
            _gameDataService = gameDataService;
            _playerService = playerService;
            _shipAlgorithmService = shipAlgorithmService;
            _gameViewModelService = gameViewModelService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BeginGame(GameViewModel viewModel)
        {
            if(!ModelState.IsValid)
            {
                return View("Index",viewModel);
            }

            if (!_playerService.CheckIfPlayersExist(viewModel))
            {
                _playerService.ProvideAccountsForPlayers(viewModel);
            }
            
            viewModel.FirstPlayerId = _playerService.GetPlayerIdByName(viewModel.FirstPlayerName);
            viewModel.CurrentPlayerId = viewModel.FirstPlayerId;
            viewModel.SecondPlayerId = _playerService.GetPlayerIdByName(viewModel.SecondPlayerName);

            if (!_gameManagementService.CheckIfPlayersAlreadyPlay(viewModel))
            {
                _gameManagementService.CreateGame(viewModel);

                var allPlayersShips = _shipAlgorithmService.GetShipsCoordsForPlayers();

                _gameDataService.InsertShipsForPlayers(allPlayersShips, viewModel);
                viewModel.currentPlayerProgress = new List<GameProgress>();
            }
            else
            {
                viewModel.currentPlayerProgress = _gameProgressService.GetPlayerProgress(viewModel.CurrentGameId, viewModel.CurrentPlayerId);
            }

            viewModel.CurrentGameId = _gameDataService.GetGameIdByPlayersId(viewModel.FirstPlayerId, viewModel.SecondPlayerId);
            viewModel.CurrentPlayerName = viewModel.FirstPlayerName;
            viewModel.OpponentPlayerId = viewModel.SecondPlayerId;
            viewModel.OpponentPlayerName = viewModel.SecondPlayerName;
            viewModel.GameFinished = false;

            return View("Game", viewModel);
        }

        [HttpPost]
        public ActionResult PerformShot(GameViewModel viewModel)
        {
            if(!ModelState.IsValid)
            {
                return View("Game", viewModel);
            }
            _gameProgressService.SavePlayerShot(viewModel);

            var playerWon = _gameProgressService.CheckIfPlayerWon(viewModel.CurrentGameId, viewModel.CurrentPlayerId);

            if(playerWon)
            {
                viewModel.GameFinished = true;
                _gameProgressService.EraseUsersGameData(viewModel.CurrentGameId, viewModel.FirstPlayerId, viewModel.SecondPlayerId);
            }
            else
            {
                viewModel = _gameViewModelService.SwapCurrentPlayers(viewModel);
                viewModel.currentPlayerProgress = _gameProgressService.GetPlayerProgress(viewModel.CurrentGameId, viewModel.CurrentPlayerId);
            }            

            return View("Game", viewModel);
        }
    }
}