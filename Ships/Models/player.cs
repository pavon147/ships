//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ships.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Player
    {
        public Player()
        {
            this.Game = new HashSet<Game>();
            this.Game1 = new HashSet<Game>();
            this.GameProgress = new HashSet<GameProgress>();
            this.PlayerShip = new HashSet<PlayerShip>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<Game> Game { get; set; }
        public virtual ICollection<Game> Game1 { get; set; }
        public virtual ICollection<GameProgress> GameProgress { get; set; }
        public virtual ICollection<PlayerShip> PlayerShip { get; set; }
    }
}
